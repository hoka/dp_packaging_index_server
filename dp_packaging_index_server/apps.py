from django.apps import AppConfig


class Config(AppConfig):
    name = 'dp_packaging_index_server'
    ask_package_storage_path = 'package_storage_path'
