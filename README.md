Information
===========
With this app you can host a package index server like pypi. It has a reduced functionality.

The common use case is hosting packages you don't want to share with other people (private or business purpose).

Take a look on https://gitlab.com/hoka/django_packaging_index_server if you need a full project template to setup the server.
...

Requirements
============
```
python3
- virtualenv
Django3
```
